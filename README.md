This is a repository of lightweight communication and marshalling ([LCM])
message type definitions for process management.

It is part of the collection of [LCM] types used on _Tethys_-class 
long-range autonomous underwater vehicles ([LRAUVs][LRAUV]) at the Monterey
Bay Aquarium Research Institute ([MBARI]).

-------------
[LCM]: http://lcm-proj.github.io/
[LRAUV]: http://www.mbari.org/auv/LRAUV.htm
[MBARI]: http://www.mbari.org/
